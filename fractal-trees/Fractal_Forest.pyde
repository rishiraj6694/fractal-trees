
def setup():
    size(900,600)
    background(255)
    noStroke()
    fill(96,128,56)
    rect(0,height-150,width,height)
    fill(135,206,235)
    rect(0,0,width,height-150)
    fill(253,184,19)
    ellipse(80,100,50,50)

N = 30
initSize = [30 + random(-10,10) for i in range(N)]
x = [random(20,880) for i in range(N)]
y = [580 + 5*(initSize[i]-40) for i in range(N)]
angle = [0 for i in range(N)]
direction = [(-1)**i for i in range(N)]
splitProb = [.02 + random(.01) for i in range(N)]
omega = .1
t = 0
dt = .1

def shrink(t):
    return 1/(1+t)**1.2

def draw():
    global x, y, t, angle
    fill(83/(1+t)**.1, 200 - 200*shrink(t)**.1, 10*shrink(t)**.1)
    for i in range(len(x)):
        ellipse(x[i], y[i], initSize[i]*shrink(t), initSize[i]*shrink(t))
        x[i] += 10*shrink(t)*sin(angle[i])
        y[i] -= 12*shrink(t)*cos(angle[i])
        angle[i] += direction[i]*omega*dt
        if random(1) < splitProb[i]:
            x.append(x[i])
            y.append(y[i])
            angle.append(angle[i])
            direction.append(-direction[i])
            splitProb.append(splitProb[i])
            initSize.append(initSize[i])
    t += dt
