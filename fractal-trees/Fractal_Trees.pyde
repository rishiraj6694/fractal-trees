
w,h = 700,600

def setup():
    size(w,h)
    background(255)
    noStroke()
    fill(0)

x = [w/2]
y = [h*.95]
v = [1]
theta = [-.1]
splits = [0]
da = .02
dx,dy = 6,7
initSize = 20
t,dt = 0,.01
splitProb = .02
splitDepth = 10
rng = 0

def shrink(t):
    return 1/(1+t)**1.5

def draw():
    global x,y,initSize,t,theta
    fill(100*shrink(t)**.5, 120 - 120*shrink(t)**.5, 20*shrink(t)**.5)
    for i in range(len(x)):
        ellipse(x[i],y[i],initSize*shrink(t),initSize*shrink(t))
        y[i] -= dy*shrink(t)*cos(theta[i])
        x[i] += dx*shrink(t)*sin(theta[i])
        theta[i] += v[i]*da + random(-rng,rng)
        if random(1) < splitProb and splits[i] < splitDepth:
            x.append(x[i])
            y.append(y[i])
            theta.append(theta[i])
            v.append(-v[i])
            splits.append(splits[i] + 1)
    t += .02
